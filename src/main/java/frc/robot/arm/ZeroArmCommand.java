package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;

public class ZeroArmCommand extends Command {
    public ZeroArmCommand() {
        addRequirements(Robot.armSubsystem);
    }   

    public void execute(){
      Robot.armSubsystem.zeroArmEncoders();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
