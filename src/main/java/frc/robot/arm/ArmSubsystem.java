package frc.robot.arm;

import com.revrobotics.CANSparkLowLevel;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ArmSubsystem extends SubsystemBase{
    private RelativeEncoder armMainEncoder;
    private RelativeEncoder armFollowerEncoder;
    private CANSparkMax armMain;
    private CANSparkMax armFollower;
    private double armMainMax = -40;
    private double armMainMin = 5;
    private double armFollowerMax = -1000;
    private double armFollowerMin = 1000;

    public ArmSubsystem() {
        armMain = new CANSparkMax(Constants.ARM_MAIN, CANSparkLowLevel.MotorType.kBrushless);
        armMainEncoder = armMain.getEncoder();
        armMainEncoder.setPosition(0); //Change back to zero
        armMain.setIdleMode(IdleMode.kBrake);
        armFollower = new CANSparkMax(Constants.ARM_FOLLOWER, CANSparkLowLevel.MotorType.kBrushless);
        armFollowerEncoder = armFollower.getEncoder();
        armFollowerEncoder.setPosition(0); // Change back to zero
        armFollower.setIdleMode(IdleMode.kBrake);
    }

    public void setMainSpeed(double speed){
        if ((speed < 0 && getMainEncoderPosition() < armMainMax) || (speed > 0 && getMainEncoderPosition() > armMainMin)) {
            armMain.set(0);
        }
        else {
            if (Math.abs(speed * Constants.MAIN_ARM_SPEED) > .5 ) {
                armMain.set(.65 * (speed/Math.abs(speed)));
            }
            else {
                armMain.set(speed * Constants.MAIN_ARM_SPEED);
            }
        }
    }

    public void setFollowerSpeed(double speed) {
        if (Math.abs(speed * Constants.MAIN_ARM_SPEED) > .5 ) {
            armFollower.set(0.45 * (speed/Math.abs(speed)));
        }
        else {
            armFollower.set(speed * Constants.MAIN_ARM_SPEED);
        }
    }

    public void stop() {
        armMain.set(0);
        armFollower.set(0);
    }

    public double getMainEncoderAngle() {
        return armMainEncoder.getPosition() * Constants.ARM_MAIN_ENCODER_CONVERSION;
    }

    public double getFollowerEncoderAngle() {
        return armFollowerEncoder.getPosition() * Constants.ARM_FOLLOWER_ENCODER_CONVERSION;
    }

    public double getMainEncoderPosition() {
        return armMainEncoder.getPosition();
    }

    public double getFollowerEncoderPosition() {
        return armFollowerEncoder.getPosition();
    }

    public void zeroArmEncoders() {
        armFollowerEncoder.setPosition(0);
        armMainEncoder.setPosition(0);
    }

    public void shooterBrake() {
        armFollower.setIdleMode(IdleMode.kBrake);
    }
    public void shooterCoast() {
        armFollower.setIdleMode(IdleMode.kCoast);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Arm Main Motor Speed ", armMainEncoder.getVelocity());
        SmartDashboard.putNumber("Arm Follower Motor Speed ", armFollowerEncoder.getVelocity());
        SmartDashboard.putNumber("Arm Main Encoder Position ", getMainEncoderPosition());
        SmartDashboard.putNumber("Arm Follower Encoder Position ", getFollowerEncoderPosition());
    }

}
