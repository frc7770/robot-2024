package frc.robot.arm;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;
import frc.robot.arm.ArmSubsystem;

public class XboxArmCommand extends Command {
    XboxController copilot;
    double pov;
    public XboxArmCommand() {
        addRequirements(Robot.armSubsystem);
        copilot = Robot.m_controller_2;
    }

    @Override
    public void execute() {
        double speed = copilot.getLeftY();
        double speed2 = copilot.getRightY();

        pov = Robot.m_controller_2.getPOV();
        if(pov == 0) {
            ToAngleCommand.shooterAdjustment -= .2;
        }
        else if(pov == 180) {
            ToAngleCommand.shooterAdjustment += .2;
        }

        // Ensure Arms don't go with climbers
        if (Math.abs(speed) < Math.abs(copilot.getLeftY())) {
            Robot.armSubsystem.setMainSpeed(0);
        }
        if (Math.abs(speed2) < Math.abs(copilot.getRightY())) {
            Robot.armSubsystem.setFollowerSpeed(0);
        }

        if (Math.abs(speed) > .15) {
            Robot.armSubsystem.setMainSpeed(speed);
        }
        else {
            Robot.armSubsystem.setMainSpeed(0);
        }
        if (Math.abs(speed2) > .15) {
            Robot.armSubsystem.setFollowerSpeed(speed2);
        }
        else {
            Robot.armSubsystem.setFollowerSpeed(0);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        Robot.armSubsystem.stop();
        pov = -1;
    }
}
