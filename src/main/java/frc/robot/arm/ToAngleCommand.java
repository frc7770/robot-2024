package frc.robot.arm;

import com.ctre.phoenix6.controls.CoastOut;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.Robot;

public class ToAngleCommand extends Command {
    double targetMainAngle = 0;
    //public static double shooterAngle = 0;
    public static double shooterAdjustment = 0;
    double targetFollowerAngle;
    double mainDiff;
    double followerDiff;
    boolean toIntake;
    boolean toStow;
    boolean toShooter;
    boolean shooterBrake;
    public ToAngleCommand(double mainAngle, double followAngle) {
        addRequirements(Robot.armSubsystem);
        targetMainAngle = mainAngle;
        targetFollowerAngle = followAngle;
        toIntake = false;
        toStow = false;
        toShooter = true;
        shooterBrake = true;
    }   

    public ToAngleCommand(double mainAngle, double followAngle, boolean movingToIntake) {
        addRequirements(Robot.armSubsystem);
        targetMainAngle = mainAngle; //This will be tuned to the actual required angle for note retrieval from the human player station
        targetFollowerAngle = followAngle;
        toIntake = movingToIntake;
        toStow = false;
        toShooter = false;
        shooterBrake = false;
        System.out.println("Created to intake boolean");
    }   

    public ToAngleCommand(boolean movingToStow) {
        addRequirements(Robot.armSubsystem);
        targetMainAngle = 0; //This will be tuned to the actual required angle for note retrieval from the human player station
        targetFollowerAngle = 0;
        toStow = movingToStow;
        toIntake = false;
        shooterBrake = false;
        toShooter = false;
    }   


    public void execute(){
        double currentMainAngle = Robot.armSubsystem.getMainEncoderPosition();
        double currentFollowerAngle = Robot.armSubsystem.getFollowerEncoderPosition();


        if(toShooter) {
            mainDiff = (targetMainAngle + shooterAdjustment) - currentMainAngle;
        }
        else {
            mainDiff = targetMainAngle - currentMainAngle;
        }
        followerDiff = targetFollowerAngle - currentFollowerAngle;


        if (shooterBrake) {
            Robot.armSubsystem.shooterBrake();
        }
        else {
            Robot.armSubsystem.shooterCoast();
        }

        //Going from stow to intake wait for follower
        if (toStow) {
            System.out.println("Moving only the main");
            Robot.armSubsystem.setMainSpeed(2.5 * (mainDiff / Math.abs(mainDiff)));//mainDiff / Constants.ARM_MAIN_SPEED_CONVERSION);
            Robot.armSubsystem.setFollowerSpeed(2.5 * followerDiff / Constants.ARM_FOLLOWER_SPEED_CONVERSION);
        }
        else if (toIntake && Math.abs(mainDiff) > 20) {
            System.out.println("Moving main and follower backwards");
            Robot.armSubsystem.setMainSpeed(1.5 * (mainDiff / Math.abs(mainDiff)));//mainDiff / Constants.ARM_MAIN_SPEED_CONVERSION);
            Robot.armSubsystem.setFollowerSpeed(-followerDiff / (Constants.ARM_FOLLOWER_SPEED_CONVERSION * 25));
        }
        /* else if (mainDiff > 10) {
            System.out.println("Moving only the main");
            Robot.armSubsystem.setMainSpeed(1.25 * (mainDiff / Math.abs(mainDiff)));//mainDiff / Constants.ARM_MAIN_SPEED_CONVERSION);
            Robot.armSubsystem.setFollowerSpeed(0);
        } */
        else if (Math.abs(mainDiff) < 5 && Math.abs(followerDiff) < 5) {
            Robot.armSubsystem.setMainSpeed(mainDiff / Constants.ARM_MAIN_SPEED_CONVERSION);
            Robot.armSubsystem.setFollowerSpeed(followerDiff / Constants.ARM_FOLLOWER_SPEED_CONVERSION);
        }
        else {
            Robot.armSubsystem.setMainSpeed(mainDiff / Constants.ARM_MAIN_SPEED_CONVERSION);
            Robot.armSubsystem.setFollowerSpeed(followerDiff / Constants.ARM_FOLLOWER_SPEED_CONVERSION);
        }
        SmartDashboard.putBoolean("Is Moving to Intake", toIntake);
        SmartDashboard.putNumber("Main Diff for toAngle", mainDiff);
        SmartDashboard.putNumber("TargetMainAngle", targetMainAngle);
        SmartDashboard.putNumber("Shooter ADjustment", shooterAdjustment);
    }
    
    @Override
    public boolean isFinished() {
        return (Math.abs(mainDiff) <= .25 && Math.abs(followerDiff) <= .25);
    }

    @Override
    public void end(boolean interrupted){
        Robot.shooterSubsystem.stop();
        toStow = false;
        toShooter = false;
        toIntake = false;
    }
}
