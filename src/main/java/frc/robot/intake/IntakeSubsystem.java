package frc.robot.intake;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeSubsystem extends SubsystemBase{
    private RelativeEncoder intakeMainEncoder;
    private RelativeEncoder intakeFollowerEncoder;
    private CANSparkMax intakeMain;
    private CANSparkMax intakeFollower;

    public IntakeSubsystem() {
        intakeMain = new CANSparkMax(Constants.INTAKE_MAIN, CANSparkLowLevel.MotorType.kBrushless);
        intakeMainEncoder = intakeMain.getEncoder();
        intakeMain.setIdleMode(IdleMode.kCoast);
    }

    public void setSpeed(double speed){
        intakeMain.set(speed);// * Constants.INTAKE_SPEED);
        //System.out.println("Intake going");
    }

    public void stop() {
        intakeMain.set(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("intake Main Motor Speed", intakeMainEncoder.getVelocity());
    }

}