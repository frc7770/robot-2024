package frc.robot.intake;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;


// turn on shooter and set to certain speed
public class AutoIntakeCommand extends Command{
  boolean backwards = false;
  double speed = 0;
  public AutoIntakeCommand() {
    addRequirements(Robot.intakeSubsystem);
    backwards = false;
    speed = 0;
  } 
  public AutoIntakeCommand(boolean reverse) {
    addRequirements(Robot.intakeSubsystem);
    backwards = reverse;
    speed = 0;
  } 
  public AutoIntakeCommand(double intakeSpeed) {
    speed = -intakeSpeed;
    backwards= false;
  }

  public void execute(){
    if (backwards) {
      Robot.intakeSubsystem.setSpeed(.25);
    }
    else if (speed > 0) {
      Robot.intakeSubsystem.setSpeed(speed);
    }
    else {
      Robot.intakeSubsystem.setSpeed(-Constants.INTAKE_SPEED);
    }
  }
  
  @Override
  public void end(boolean interrupted){
    Robot.intakeSubsystem.stop();
    speed = 0;
  }
}
