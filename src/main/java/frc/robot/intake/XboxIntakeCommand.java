package frc.robot.intake;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.intake.IntakeSubsystem;

public class XboxIntakeCommand extends Command {

    private IntakeSubsystem intake;
    private XboxController xbox;

    public XboxIntakeCommand() {
        addRequirements(Robot.intakeSubsystem);
        intake = Robot.intakeSubsystem;
        xbox = Robot.m_controller;
    }

    @Override
    public void execute() {
        double speed1 = xbox.getRightTriggerAxis();
        double speed2 = xbox.getLeftTriggerAxis();
        if (speed1 > 0.1) {
            intake.setSpeed(-1);
        } else if (speed2 > 0.1) {
            intake.setSpeed(1);
            
        } else {
            intake.stop();
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        intake.stop();
    }
}
