// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.List;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.PathPlannerAuto;
import com.pathplanner.lib.path.PathPlannerPath;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.vision.VisionThread;
import edu.wpi.first.wpilibj.DataLogManager;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.AutoConstants;
import frc.robot.Constants.DriveConstants;
import frc.robot.arm.ToAngleCommand;
import frc.robot.arm.ArmSubsystem;
import frc.robot.arm.XboxArmCommand;
import frc.robot.arm.ZeroArmCommand;
import frc.robot.climber.ClimberDownCommand;
import frc.robot.climber.ClimberSubsystem;
import frc.robot.climber.ClimberUpCommand;
import frc.robot.climber.XboxClimberCommand;
import frc.robot.climber.ZeroClimberCommand;
import frc.robot.drive.Drivetrain;
import frc.robot.drive.ZeroGyroCommand;
import frc.robot.intake.AutoIntakeCommand;
import frc.robot.intake.IntakeSubsystem;
import frc.robot.led.LEDController;
import frc.robot.sensors.ColorSensor;
import frc.robot.intake.XboxIntakeCommand;
import frc.robot.led.LEDController;
import frc.robot.shooter.ReverseCommand;
import frc.robot.shooter.ShooterCommand;
import frc.robot.shooter.ShooterSubsystem;
import frc.robot.shooter.SlowShotCommand;
import frc.robot.vision.AprilTagTagetDetection;
import frc.robot.vision.OrangeNoteDetection;
import frc.robot.vision.VisionThreadSensor;


public class Robot extends TimedRobot {
  public static Robot robot = new Robot();
  public static final XboxController m_controller = new XboxController(0);
  public static final XboxController m_controller_2 = new XboxController(1);

  public static LEDController testLEDs;

  public static JoystickButton aButton = new JoystickButton(m_controller, 1);
  public static JoystickButton bButton = new JoystickButton(m_controller, 2);
  public static JoystickButton xButton = new JoystickButton(m_controller, 3);
  public static JoystickButton yButton = new JoystickButton(m_controller, 4);
  public static JoystickButton lbButton = new JoystickButton(m_controller, 5);
  public static JoystickButton rbButton = new JoystickButton(m_controller, 6);
  public static JoystickButton backButton = new JoystickButton(m_controller, 7);
  public static JoystickButton startButton = new JoystickButton(m_controller, 8);
  public static JoystickButton leftJoystickPress = new JoystickButton(m_controller, 9);
  public static JoystickButton rightJoystickPress = new JoystickButton(m_controller, 10);

  public static JoystickButton aButton2 = new JoystickButton(m_controller_2, 1);
  public static JoystickButton bButton2 = new JoystickButton(m_controller_2, 2);
  public static JoystickButton xButton2 = new JoystickButton(m_controller_2, 3);
  public static JoystickButton yButton2 = new JoystickButton(m_controller_2, 4);
  public static JoystickButton lbButton2 = new JoystickButton(m_controller_2, 5);
  public static JoystickButton rbButton2 = new JoystickButton(m_controller_2, 6);
  public static JoystickButton backButton2 = new JoystickButton(m_controller_2, 7);
  public static JoystickButton startButton2 = new JoystickButton(m_controller_2, 8);
  public static JoystickButton leftJoystickPress2 = new JoystickButton(m_controller_2, 9);
  public static JoystickButton rightJoystickPress2 = new JoystickButton(m_controller_2, 10);
  public static final Drivetrain m_swerve = new Drivetrain(robot);
  //public static Drivetrain m_swerve;
  private Command m_autonomousCommand;

  // Slew rate limiters to make joystick inputs more gentle; 1/3 sec from 0 to 1.
  private final SlewRateLimiter m_xspeedLimiter = new SlewRateLimiter(3);
  private final SlewRateLimiter m_yspeedLimiter = new SlewRateLimiter(3);
  private final SlewRateLimiter m_rotLimiter = new SlewRateLimiter(3);

  // Our subsystems
  public static ShooterSubsystem shooterSubsystem = new ShooterSubsystem();
  public static IntakeSubsystem intakeSubsystem = new IntakeSubsystem();
  public static ClimberSubsystem climberSubsystem = new ClimberSubsystem();
  public static ArmSubsystem armSubsystem = new ArmSubsystem();

  //Colors
  Color pinkyPie = new Color(100, 20, 50);

  //ColorSensor
  //ColorSensor colorSensor = new ColorSensor();

  //Vision
  public static VisionThreadSensor vision;

  //Camera
  public static UsbCamera camera0;

  //Auto Chooser
  private static SendableChooser<Command> autoChooser;

  @Override 
  public void robotInit() {
    NamedCommands.registerCommand("Shooter Command", getAutoShooter());
    NamedCommands.registerCommand("Auto Intake Command", getAutoIntake());//new AutoIntakeCommand().withTimeout(2));
    NamedCommands.registerCommand("Stow Command", stowCommand());
    autoChooser = AutoBuilder.buildAutoChooser();

    // subsystem settings
    intakeSubsystem.setDefaultCommand(new XboxIntakeCommand());
    //shooterSubsystem.setDefaultCommand();
    climberSubsystem.setDefaultCommand(new XboxClimberCommand());
    armSubsystem.setDefaultCommand(new XboxArmCommand());

    //setTestLEDS(pinkyPie);//Color.kGreen);

    //camera0 = CameraServer.startAutomaticCapture(0);
    //camera0.setResolution(640, 480);
    //camera0.setFPS(30);

    //vision = new VisionThreadSensor(camera0, new OrangeNoteDetection());
    //vision = new VisionThreadSensor(camera0, new AprilTagTagetDetection());
    //final VisionThread visionThread = new VisionThread(camera0, new AprilTagTagetDetection(), pipeline -> {

    //});
    //visionThread.start();

    //DataLogManager.start();

    //DriverStation.startDataLog(DataLogManager.getLog());

    SmartDashboard.putData("Auto Chooser", autoChooser);
  }


  @Override 
  public void disabledPeriodic() {

  }
  
  @Override
  public void robotPeriodic() {
        if(m_swerve!=null) {
            m_swerve.logToDashboard(); 
        }
    CommandScheduler.getInstance().run(); 
    //colorSensor.logToDashboard();
  }

  @Override
  public void autonomousInit(){
    // init drive config
    if(m_swerve!=null) {
      m_autonomousCommand = getPathPlannerCommand();// = getAutonomousCommand();
    }

    // schedule the autonomous command (example)
    if (m_autonomousCommand != null) {
        m_autonomousCommand.schedule();
    }
  }
  @Override
  public void autonomousPeriodic() {
    // driveWithJoystick(true);
    m_swerve.updateOdometry();
  }

  @Override
  public void teleopInit(){
    configureButtonBindings();
  }

  @Override
  public void teleopPeriodic() {
    // drive settings
    if(m_swerve!=null) {
      driveWithJoystick(true);
      m_swerve.updateOdometry();
    }
    //driveSingle();

    /*m_swerve.m_frontRight.m_turningController.setVoltage(1);
     m_swerve.m_backRight.m_turningController.setVoltage(1);
      m_swerve.m_frontLeft.m_turningController.setVoltage(1);
       m_swerve.m_backLeft.m_turningController.setVoltage(1);*/
       //shooterSubsystem.logToDashboard();
       armSubsystem.logToDashboard();
       climberSubsystem.logToDashboard();
  }

  private void configureButtonBindings() {
    // First Controller
    aButton.onTrue(shootCommand());
    //bButton.onTrue(new ReverseCommand().withTimeout(2));

    bButton.onTrue(stowCommand()); // Stow Command
    yButton.onTrue(shooterHeightCommand()); //Shoot Level
    xButton.onTrue(intakeHeightCommand()); //Ground Note Retrieval Position
    rbButton.onTrue(new ShooterCommand().withTimeout(2));

    // init drive buttons
    if(m_swerve!=null){      
      startButton.onTrue(new ZeroGyroCommand());
    }

    //Second Controller
    aButton2.onTrue(new ZeroClimberCommand());
    bButton2.onTrue(new ZeroArmCommand());
    //rbButton2.onTrue(new ClimberUpCommand());
    //lbButton2.onTrue(new ClimberDownCommand());
    //startButton2.onTrue(new ParallelCommandGroup(new SlowShotCommand(), new SequentialCommandGroup(new AutoIntakeCommand(true).withTimeout(.2), new WaitCommand(.0), new AutoIntakeCommand(1))).withTimeout(2));
  }

  public SequentialCommandGroup getAutoShooter() {
    return new SequentialCommandGroup(new WaitCommand(.5), new ZeroArmCommand(), shooterHeightCommand().withTimeout(2), new WaitCommand(0), shootCommand());
  }

  public SequentialCommandGroup getAutoIntake() {
    return new SequentialCommandGroup(intakeHeightCommand(), new AutoIntakeCommand(1).withTimeout(7));
  }
  public Command shooterHeightCommand() {
    return (new ToAngleCommand(-8, -14).withTimeout(2));
  }

  public Command intakeHeightCommand() {
    return (new ToAngleCommand(-39, 20, true).withTimeout(3));
  }

  public Command stowCommand() {
    return (new ToAngleCommand(true).withTimeout(2));
  }

  public Command shootCommand() {
    return new ParallelCommandGroup(new ShooterCommand(), new SequentialCommandGroup(new AutoIntakeCommand(true).withTimeout(.75), new AutoIntakeCommand())).withTimeout(2);
  }

  private void driveWithJoystick(boolean fieldRelative) {
    // Get the x speed. We are inverting this because Xbox controllers return
    // negative values when we push forward.
    final double xSpeed =
        -m_xspeedLimiter.calculate(MathUtil.applyDeadband(m_controller.getLeftY(), 0.05))
            * Drivetrain.kMaxSpeed;
    // Get the y speed or sideways/strafe speed. We are inverting this because
    // we want a positive value when we pull to the left. Xbox controllers
    // return positive values when you pull to the right by default.
    final double ySpeed =
        -m_yspeedLimiter.calculate(MathUtil.applyDeadband(m_controller.getLeftX(), 0.05))
            * Drivetrain.kMaxSpeed;
    // Get the rate of angular rotation. We are inverting this because we want a
    // positive value when we pull to the left (remember, CCW is positive in
    // mathematics). Xbox controllers return positive values when you pull to
    // the right by default.
    final double rot =
        -m_rotLimiter.calculate(MathUtil.applyDeadband(m_controller.getRightX(), 0.025))
            * Drivetrain.kMaxAngularSpeed;

    m_swerve.drive(xSpeed, ySpeed, rot, fieldRelative, getPeriod());
  }

   public Command getAutonomousCommand() {
        // 1. Create trajectory settings
        TrajectoryConfig trajectoryConfig = new TrajectoryConfig(
                AutoConstants.kMaxSpeedMetersPerSecond,
                AutoConstants.kMaxAccelerationMetersPerSecondSquared)
                        .setKinematics(DriveConstants.kDriveKinematics);

        // 2. Generate trajectory
        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
                new Pose2d(0, 0, new Rotation2d(0)),
                List.of(
                        new Translation2d(1, 0),
                        new Translation2d(1, -1),
                        new Translation2d(0,-1)),
                new Pose2d(0, 0, Rotation2d.fromDegrees(0)),
                trajectoryConfig);

        // 3. Define PID controllers for tracking trajectory
        PIDController xController = new PIDController(AutoConstants.kPXController, 0, 0);
        PIDController yController = new PIDController(AutoConstants.kPYController, 0, 0);
        ProfiledPIDController thetaController = new ProfiledPIDController(
                AutoConstants.kPThetaController, 0, 0, AutoConstants.kThetaControllerConstraints);
        thetaController.enableContinuousInput(-Math.PI, Math.PI);

        // 4. Construct command to follow trajectory
        SwerveControllerCommand swerveControllerCommand = new SwerveControllerCommand(
                trajectory,
                m_swerve::getPose,
                DriveConstants.kDriveKinematics,
                xController,
                yController,
                thetaController,
                m_swerve::setModuleStates,
                m_swerve);

        // 5. Add some init and wrap-up, and return everything
        return new SequentialCommandGroup(
               // new InstantCommand(() -> swerveSubsystem.resetOdometry(trajectory.getInitialPose())),
                swerveControllerCommand,
                new InstantCommand(() -> m_swerve.stopModules()));
              // return swerveControllerCommand; 
    }

    public Command getPathPlannerCommand() {
      return autoChooser.getSelected();
    }

    //public Command getPathPlannerCommand() {
      //return autoChooser.getSelected();
    //}

    public void setTestLEDS(Color color) {
        System.out.println("Changing LED Colors");
        try {
          if (testLEDs == null) {
            testLEDs = new LEDController(0, 145); //two 31s one 27 and a 56 (57?)
          }
          testLEDs.solid(color);
        } catch (Exception e) {
          //TODO: handle exception
        }
      }
    
}
