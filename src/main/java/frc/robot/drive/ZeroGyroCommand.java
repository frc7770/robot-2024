package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;

public class ZeroGyroCommand extends Command {
    public ZeroGyroCommand() {
        addRequirements(Robot.m_swerve);
    }   

    public void execute(){
      Robot.m_swerve.zeroGyro();
  
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
