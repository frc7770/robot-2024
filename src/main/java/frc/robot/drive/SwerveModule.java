// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.drive;

import com.ctre.phoenix6.hardware.CANcoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SwerveModule {
  private static final double kWheelRadius = 0.0508;
  private static final int kEncoderResolution = 4096;

  private static final double kModuleMaxAngularVelocity = Drivetrain.kMaxAngularSpeed;
  private static final double kModuleMaxAngularAcceleration = 5 * Math.PI; // radians per second squared

  public int turningEncoderID;

  // original setup
  // private final MotorController m_driveMotor;
  // private final MotorController m_turningMotor;
  // private final Encoder m_driveEncoder;
  // private final Encoder m_turningEncoder;

  public final CANSparkMax m_driveController;
  public final CANSparkMax m_turningController;
  private final RelativeEncoder m_driveEncoder;
  private final CANcoder m_turningEncoder;

  // Gains are for example purposes only - must be determined for your own robot!
  private final PIDController m_drivePIDController = new PIDController(
    0.1000,
    0,
    0
    );

  // Gains are for example purposes only - must be determined for your own robot!
  private final ProfiledPIDController m_turningPIDController = new ProfiledPIDController(
      4.25, // 1
      0,
      0,
      new TrapezoidProfile.Constraints(
          kModuleMaxAngularVelocity, kModuleMaxAngularAcceleration));

  // Gains are for example purposes only - must be determined for your own robot!
  private final SimpleMotorFeedforward m_driveFeedforward = new SimpleMotorFeedforward(1, 3);
  private final SimpleMotorFeedforward m_turnFeedforward = new SimpleMotorFeedforward(0.1, 0.05);

  /**
   * Constructs a SwerveModule with a drive motor, turning motor, drive encoder
   * and turning encoder.
   *
   * @param driveMotorChannel      PWM output for the drive motor.
   * @param turningMotorChannel    PWM output for the turning motor.
   * @param driveEncoderChannelA   DIO input for the drive encoder channel A
   * @param driveEncoderChannelB   DIO input for the drive encoder channel B
   * @param turningEncoderChannelA DIO input for the turning encoder channel A
   * @param turningEncoderChannelB DIO input for the turning encoder channel B
   */
  public SwerveModule(
      int driveMotorChannel,
      int turningMotorChannel,
      int turningEncoderChannel) {

    // original setup
    // m_driveMotor = new PWMSparkMax(driveMotorChannel);
    // m_turningMotor = new PWMSparkMax(turningMotorChannel);
    // m_driveEncoder = new Encoder(driveEncoderChannelA, driveEncoderChannelB);
    // m_turningEncoder = new Encoder(turningEncoderChannelA,
    // turningEncoderChannelB);

    m_driveController = new CANSparkMax(driveMotorChannel, CANSparkMax.MotorType.kBrushless);
    m_turningController = new CANSparkMax(turningMotorChannel, CANSparkMax.MotorType.kBrushless);
    m_driveEncoder = m_driveController.getEncoder(); // new Encoder(driveEncoderChannelA, driveEncoderChannelB);
    m_turningEncoder = new CANcoder(turningEncoderChannel);
    turningEncoderID = turningEncoderChannel;


    
    // Set the distance per pulse for the drive encoder. We can simply use the
    // distance traveled for one rotation of the wheel divided by the encoder
    // resolution.
    // TODO: Adapt this for 7770 setup
    // m_driveEncoder.setDistancePerPulse(2 * Math.PI * kWheelRadius /
    // kEncoderResolution);
    m_driveEncoder.setPositionConversionFactor(2 * Math.PI * kWheelRadius / kEncoderResolution * 624.4707);

    // Set the distance (in this case, angle) in radians per pulse for the turning
    // encoder.
    // This is the the angle through an entire rotation (2 * pi) divided by the
    // encoder resolution.
    // TODO: Adapt this for 7770 setup
    // m_turningEncoder.setDistancePerPulse(2 * Math.PI / kEncoderResolution);

    // Limit the PID Controller's input range between -pi and pi and set the input
    // to be continuous.
    m_turningPIDController.enableContinuousInput(-Math.PI, Math.PI);
    //m_turningPIDController.enableContinuousInput(0,1);
  }

  /**
   * Returns the current state of the module.
   *
   * @return The current state of the module.
   */
  public SwerveModuleState getState() {
    return new SwerveModuleState(
      // original setup
      //m_driveEncoder.getRate(), new Rotation2d(m_turningEncoder.getDistance()));
        m_driveEncoder.getVelocity(), new Rotation2d(getPositionRadians()));
  }

  /**
   * Returns the current position of the module.
   *
   * @return The current position of the module.
   */
  public SwerveModulePosition getPosition() {
    return new SwerveModulePosition(
      // original setup
      //m_driveEncoder.getDistance(), new Rotation2d(m_turningEncoder.getDistance()));
        m_driveEncoder.getPosition(), new Rotation2d(getPositionRadians()));
  }
 //Returning the current position of the turning encoder
  public double getAbsolutePosition() {
    return m_turningEncoder.getAbsolutePosition().getValue();
  }

  public CANcoder getEncoder() {
    return m_turningEncoder;
  }
  
  public RelativeEncoder getDriveEncoder(){
    return m_driveEncoder;
    

  } 

  public double getPositionRadians() {
    //return   ( (m_turningEncoder.getAbsolutePosition().getValue() * 2 * Math.PI)+ Math.PI) % (Math.PI);
    double returnOut;
    returnOut = m_turningEncoder.getAbsolutePosition().getValue() * 2 * Math.PI;
    if (returnOut> Math.PI){
      returnOut -= 2* Math.PI;
    }
    return returnOut;
  }

  public void stop() {
    m_driveController.set(0);
    m_driveController.set(0);
}

  public SwerveModuleState optimize(
      SwerveModuleState desiredState, Rotation2d currentAngle) {
    var delta = desiredState.angle.minus(currentAngle);
    //SmartDashboard.putNumber("Delta of " + turningEncoderID + " ", delta.getDegrees());
    if (Math.abs(delta.getDegrees()) > 90.0) {
      return new SwerveModuleState(
          -desiredState.speedMetersPerSecond,
          desiredState.angle.rotateBy(Rotation2d.fromDegrees(180.0)));
          
    } else {
      return new SwerveModuleState(desiredState.speedMetersPerSecond, desiredState.angle);
    }
  }

  /**
   * Sets the desired state for the module.
   *
   * @param desiredState Desired state with speed and angle.
   */
  public void setDesiredState(SwerveModuleState desiredState) {
    // original setup
    //SwerveModuleState state = SwerveModuleState.optimize(desiredState, new Rotation2d(m_turningEncoder.getDistance()));

    var encoderRotation = new Rotation2d(getPositionRadians());
    //SmartDashboard.putNumber("Encoder Rotation Radians ", encoderRotation.getRadians());

    // Optimize the reference state to avoid spinning further than 90 degrees
    SwerveModuleState state = optimize(desiredState, encoderRotation);
    //SmartDashboard.putNumber("Desired State Value " + turningEncoderID + " ", desiredState.angle.getRadians());
    //SmartDashboard.putNumber("Sate Value " + turningEncoderID + " ", state.angle.getRadians());
    //SmartDashboard.putNumber("State Value 1 " + turningEncoderID + " ", state.angle.getRadians());
     
    //SmartDashboard.putNumber("Encoder Velocity " + turningEncoderID + " ", m_driveEncoder.getVelocity());
    // Scale speed by cosine of angle error. This scales down movement perpendicular
    // to the desired
    // direction of travel that can occur when modules change directions. This
    // results in smoother
    // driving.
    
    //System.out.println(this.turningEncoderID + " Swerve Diff " + (Math.abs(desiredState.angle.minus(encoderRotation).getDegrees()) % 90));
    
    // Dead zone code don't know if it works
    /*if ((Math.abs(desiredState.angle.minus(encoderRotation).getDegrees()) % 80) > 30) {
      state.speedMetersPerSecond *= 0;
    }
    else {
      state.speedMetersPerSecond *= state.angle.minus(encoderRotation).getCos();
    }*/
    
    state.speedMetersPerSecond *= state.angle.minus(encoderRotation).getCos();

    // Calculate the drive output from the drive PID controller.
    // original setup
    //final double driveOutput = m_drivePIDController.calculate(m_driveEncoder.getRate(), state.speedMetersPerSecond);

    final double driveOutput = m_drivePIDController.calculate(m_driveEncoder.getVelocity(), state.speedMetersPerSecond);
    final double driveFeedforward = m_driveFeedforward.calculate(state.speedMetersPerSecond);
  m_driveController.setVoltage(driveOutput + driveFeedforward);
    // Calculate the turning motor output from the turning PID controller.
    // original setup
    //final double turnOutput = m_turningPIDController.calculate(m_turningEncoder.getDistance(), state.angle.getRadians());

    final double turnOutput = m_turningPIDController.calculate(getPositionRadians(),
        state.angle.getRadians());
    //SmartDashboard.putNumber("turning PID vaule  "+ this.turningEncoderID , m_turningPIDController.calculate(getPositionRadians()));    
    //SmartDashboard.putNumber("State angle radians  "+ this.turningEncoderID, ((state.angle.getRadians())*180) / Math.PI );
    //SmartDashboard.putNumber("Turn Output  " + this.turningEncoderID, turnOutput);
    final double turnFeedforward = m_turnFeedforward.calculate(state.angle.getRadians());//m_turningPIDController.getSetpoint().velocity);
    //SmartDashboard.putNumber("PID in vaule  "+ this.turningEncoderID, (getPositionRadians())*180 / Math.PI);
    //SmartDashboard.putNumber("Turn Feed Forward  "+ this.turningEncoderID, turnFeedforward);
    //SmartDashboard.putNumber("Turn Feed Forward", turnFeedforward * 100);
    //System.out.println(this.turningEncoderID + " Output " + turnOutput);
    m_turningController.setVoltage(turnOutput);// + turnFeedforward);
  }
}
