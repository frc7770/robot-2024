// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.drive;

import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.Vector;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.Constants.DriveConstants;

import javax.sql.rowset.serial.SerialArray;

import com.kauailabs.navx.frc.AHRS;
import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


/** Represents a swerve drive style drivetrain. */
public class Drivetrain extends SubsystemBase {
  private Robot robot;

  public static final double kMaxSpeed = 4; // 3 meters per second
  public static final double kMaxAngularSpeed = Math.PI * 1; // 1/2 rotation per second

  private final Translation2d m_frontLeftLocation = new Translation2d(0.381, 0.381);
  private final Translation2d m_frontRightLocation = new Translation2d(0.381, -0.381);
  private final Translation2d m_backLeftLocation = new Translation2d(-0.381, 0.381);
  private final Translation2d m_backRightLocation = new Translation2d(-0.381, -0.381);
  
  //private final CANSparkMax m_frontLeft = new CANSparkMax;
  public final SwerveModule m_frontLeft = new SwerveModule(Constants.DRIVE_MOTOR_LEFT_FRONT, Constants.TURNING_MOTOR_LEFT_FRONT, Constants.TURNING_ENCODER_LEFT_FRONT);
  
  public final SwerveModule m_frontRight = new SwerveModule(Constants.DRIVE_MOTOR_RIGHT_FRONT, Constants.TURNING_MOTOR_RIGHT_FRONT, Constants.TURNING_ENCODER_RIGHT_FRONT);

  public final SwerveModule m_backLeft = new SwerveModule(Constants.DRIVE_MOTOR_LEFT_REAR, Constants.TURNING_MOTOR_LEFT_REAR, Constants.TURNING_ENCODER_LEFT_REAR);
  
  public final SwerveModule m_backRight = new SwerveModule(Constants.DRIVE_MOTOR_RIGHT_REAR, Constants.TURNING_MOTOR_RIGHT_REAR, Constants.TURNING_ENCODER_RIGHT_REAR);
  private final SwerveModule[] modules = new SwerveModule[] {m_frontLeft, m_frontRight, m_backLeft, m_backRight };
  private SwerveModuleState[] desiredStates = new SwerveModuleState[4];
  
  public static AHRS m_gyro = new AHRS(SPI.Port.kMXP);
  //private final AnalogGyro m_gyro = new AnalogGyro(0);

  private final Field2d m_field = new Field2d();
  private final SwerveDriveKinematics m_kinematics =
      new SwerveDriveKinematics(
         //m_frontLeftLocation, m_frontRightLocation, m_backLeftLocation, m_backRightLocation);
         new Translation2d(0.381, 0.381), new Translation2d(0.381, -0.381), new Translation2d(-0.381, 0.381) , new Translation2d(-0.381, -0.381));

  private final SwerveDriveOdometry m_odometry =
      new SwerveDriveOdometry(
          m_kinematics,
          m_gyro.getRotation2d(),
          new SwerveModulePosition[] {
            m_frontLeft.getPosition(),
            m_frontRight.getPosition(),
            m_backLeft.getPosition(),
            m_backRight.getPosition()
            
          });
  private final SwerveDrivePoseEstimator swerveDrivePoseEstimator;

  /**
   * Standard deviations of model states. Increase these numbers to trust your
   * model's state estimates less. This
   * matrix is in the form [x, y, thera]^t, with units in meters and radians, then
   * meters.
   */
  private static final Vector<N3> stateStdDevs = VecBuilder.fill(0.05, 0.05, Units.degreesToRadians(5));

  /**
   * Standard deviations of the vision measurements. Increase these numbers to
   * trust global measurements from vision
   * less. This matrix is in the form [x, y, thera]^t, with units in meters and
   * radians.
   */
  private static final Vector<N3> visionMeasurementStdDevs = VecBuilder.fill(0.5, 0.5, Units.degreesToRadians(10));

  public Drivetrain(Robot robot) {
    this.robot = robot;
    /* Communicate w/navX MXP via the MXP SPI Bus.                                     */
            /* Alternatively:  I2C.Port.kMXP, SerialPort.Port.kMXP or SerialPort.Port.kUSB     */
            /* See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface/ for details. */
            //m_gyro = new AHRS(SPI.Port.kMXP);
    m_gyro.reset();

    m_backLeft.m_turningController.setInverted(true);
    m_backRight.m_turningController.setInverted(true);
    m_frontLeft.m_turningController.setInverted(true);
    m_frontRight.m_turningController.setInverted(true);
    m_backLeft.m_driveController.setInverted(true);
    m_backRight.m_driveController.setInverted(true);
    m_frontLeft.m_driveController.setInverted(true);
    m_frontRight.m_driveController.setInverted(true);
    
    m_backLeft.m_turningController.setIdleMode(IdleMode.kCoast);
    m_backRight.m_turningController.setIdleMode(IdleMode.kCoast);
    m_frontLeft.m_turningController.setIdleMode(IdleMode.kCoast);
    m_frontRight.m_turningController.setIdleMode(IdleMode.kCoast);
    m_backLeft.m_driveController.setIdleMode(IdleMode.kCoast);
    m_backRight.m_driveController.setIdleMode(IdleMode.kCoast);
    m_frontLeft.m_driveController.setIdleMode(IdleMode.kCoast);
    m_frontRight.m_driveController.setIdleMode(IdleMode.kCoast);

    SmartDashboard.putData("Field",m_field);

    swerveDrivePoseEstimator = createSwerveDriveEstimator();
    configurePathPlanner();
  }

  /**
   * Method to drive the robot using joystick info.
   *
   * @param xSpeed Speed of the robot in the x direction (forward).
   * @param ySpeed Speed of the robot in the y direction (sideways).
   * @param rot Angular rate of the robot.
   * @param fieldRelative Whether the provided x and y speeds are relative to the field.
   */
  public void drive(
      double xSpeed, double ySpeed, double rot, boolean fieldRelative, double periodSeconds) {
    SmartDashboard.putNumber("ySpeed", ySpeed);
    SmartDashboard.putNumber("xSpeed", xSpeed);
    SmartDashboard.putNumber("Rot", rot);
    var swerveModuleStates =
        m_kinematics.toSwerveModuleStates(
            ChassisSpeeds.discretize(
                fieldRelative
                    ? ChassisSpeeds.fromFieldRelativeSpeeds(
                        xSpeed, ySpeed, rot, m_gyro.getRotation2d())
                    : new ChassisSpeeds(xSpeed, ySpeed, rot),
                periodSeconds));
    SwerveDriveKinematics.desaturateWheelSpeeds(swerveModuleStates, kMaxSpeed);
    SmartDashboard.putNumber("Rotation Angle motor 1", swerveModuleStates[1].angle.getDegrees());
    m_frontLeft.setDesiredState(swerveModuleStates[0]);
    m_frontRight.setDesiredState(swerveModuleStates[1]);
    m_backLeft.setDesiredState(swerveModuleStates[2]);
    m_backRight.setDesiredState(swerveModuleStates[3]);
    desiredStates = swerveModuleStates;
  }

  public boolean allZeroed() {
    return Math.abs(m_frontLeft.getPositionRadians() - Constants.FRONTLEFT_STARTINGRADIANS) < 0.1 && Math.abs(m_frontRight.getPositionRadians() - Constants.FRONTRIGHT_STARTINGRADIANS) < 0.1 && Math.abs(m_backLeft.getPositionRadians() - Constants.BACKLEFT_STARTINGRADIANS) < 0.1 && Math.abs(m_backRight.getPositionRadians() - Constants.BACKRIGHT_STARTINGRADIANS) < 0.1;
  }

  /** Updates the field relative position of the robot. */
  public void updateOdometry() {
    m_odometry.update(
        m_gyro.getRotation2d(),
        new SwerveModulePosition[] {
          m_frontLeft.getPosition(),
          m_frontRight.getPosition(),
          m_backLeft.getPosition(),
          m_backRight.getPosition()
        });
        m_field.setRobotPose(m_odometry.getPoseMeters());
  }

  public void setPose(Pose2d pose) {
    m_odometry.resetPosition(
        m_gyro.getRotation2d(),
        new SwerveModulePosition[] {
          m_frontLeft.getPosition(),
          m_frontRight.getPosition(),
          m_backLeft.getPosition(),
          m_backRight.getPosition()
        },
        pose);
        m_field.setRobotPose(m_odometry.getPoseMeters());
  }
//Logging the encoder positions of each of the swerve modules
  public void logToDashboard(){
    //SmartDashboard.putNumber("Front Left Absolute Position", m_frontLeft.getAbsolutePosition());
    //SmartDashboard.putNumber("Front Right Absolute Position", m_frontRight.getAbsolutePosition());
    //SmartDashboard.putNumber("Back Left Absolute Position", m_backLeft.getAbsolutePosition());
    //SmartDashboard.putNumber("Back Right Absolute Position", m_backRight.getAbsolutePosition());
    //SmartDashboard.putNumber("Back Left Drive Position", m_backLeft.getDriveEncoder().getPosition() ); //
    //SmartDashboard.putNumber("Back Right Drive Position", m_backRight.getDriveEncoder().getPosition() );
    //SmartDashboard.putNumber("Front Left Drive Position", m_frontLeft.getDriveEncoder().getPosition() );
    //SmartDashboard.putNumber("Front Right Drive Position", m_frontRight.getDriveEncoder().getPosition() );
    //SmartDashboard.putNumber("Front Left Absolute Radians", m_frontLeft.getAbsolutePosition()*2*Math.PI);
    //SmartDashboard.putNumber("Front Right Absolute Radians", m_frontRight.getAbsolutePosition()*2*Math.PI);
    //SmartDashboard.putNumber("Back Left Absolute Radians", m_backLeft.getAbsolutePosition()*2*Math.PI);
    //SmartDashboard.putNumber("Back Right Absolute Radians", m_backRight.getAbsolutePosition()*2*Math.PI);
    // Lines 122-125 were SmartDashboard.putNumber("Front Right Drive Position", m_frontRight.getEncoder().getPosition().getValue()* 2 * Math.PI );
    //SmartDashboard.putNumber("Front Left Position", m_frontLeft.getEncoder().getPosition().getValue());               //was m_frontLeft.getPosition()
    //SmartDashboard.putNumber("Front Right Position", m_frontRight.getEncoder().getPosition().getValue());
    //SmartDashboard.putNumber("Back Left Position", m_backLeft.getEncoder().getPosition().getValue());
    //SmartDashboard.putNumber("Back Right Position", m_backRight.getEncoder().getPosition().getValue()); 
    SmartDashboard.putNumber("Gyro Angle", m_gyro.getAngle() );

    SmartDashboard.putNumberArray("swerve/measuredStates", statesToArray(getModuleStates()));
    SmartDashboard.putNumberArray("swerve/desiredStates", statesToArray(getDesiredModuleStates()));
    SmartDashboard.putNumber("swerve/robotRotation", m_gyro.getRotation2d().getDegrees());
  }

  public SwerveModuleState[] getModuleStates() {
            SwerveModuleState[] states = new SwerveModuleState[4];
            int i=0;
            for (SwerveModule module : modules) {
                states[i++] = module.getState();
            }
            return states;
      }
      public SwerveModuleState[] getDesiredModuleStates() {
        return this.desiredStates;
      }
    
      
      public double[] statesToArray(SwerveModuleState[] states) {
        double[] nums = new double[states.length*2];
        int i=0;
        for(SwerveModuleState state : states) {
          if(state != null) {
            nums[i++] = state.angle.getDegrees();
            nums[i++] = state.speedMetersPerSecond;
          }
        }
        return nums;
     }

     public Pose2d getPose(){
      return m_odometry.getPoseMeters();
     }
     public Pose2d getManualPose() {
      return (m_odometry.getPoseMeters()).plus(new Transform2d(1.37, 5.55, new Rotation2d(0)));
     }

     public void setModuleStates(SwerveModuleState[] desiredStates) {
        SwerveDriveKinematics.desaturateWheelSpeeds(desiredStates, DriveConstants.kPhysicalMaxSpeedMetersPerSecond);
        m_frontLeft.setDesiredState(desiredStates[0]);
        m_frontRight.setDesiredState(desiredStates[1]);
        m_backLeft.setDesiredState(desiredStates[2]);
        m_backRight.setDesiredState(desiredStates[3]);
    }

    // public void resetOdometry(Pose2d pose) {
    //  m_odometry.resetPosition(pose, pose.getRotation());
 // }

 public void stopModules() {
  m_frontLeft.stop();
  m_frontRight.stop();
  m_backLeft.stop();
  m_backRight.stop();
}

public void zeroGyro() {
  m_gyro.reset();
}

public SwerveModulePosition[] getModulePositions() {
  SwerveModulePosition[] positions = new SwerveModulePosition[4];
  int i = 0;
  for (SwerveModule module : modules) {
    positions[i++] = module.getPosition();
  }
  return positions;
}

private void configurePathPlanner() {
  AutoBuilder.configureHolonomic(
    this::getPose, // Robot pose supplier
    this::setPose, // Method to reset odometry (will be called if your auto has a starting pose)
    this::getChassisSpeeds, // ChassisSpeeds supplier. MUST BE ROBOT RELATIVE
    //this::driveRobotRelative, // Method that will drive the robot given ROBOT RELATIVE ChassisSpeeds
    this::drivePathPlanner,
    new HolonomicPathFollowerConfig( // HolonomicPathFollowerConfig, this should likely live in your Constants class
            new PIDConstants(2.0, 0.0, 0.0), // Translation PID constants
            new PIDConstants(.4, 0.0, 0.0), // Rotation PID constants
            kMaxSpeed, // Max module sp6eed, in m/s
            0.343, // Drive base radius in meters. Distance from robot center to furthest module.
            new ReplanningConfig() // Default path replanning config. See the API for the options here
    ),
    () -> {
      // Boolean supplier that controls when the path will be mirrored for the red alliance
      // This will flip the path being followed to the red side of the field.
      // THE ORIGIN WILL REMAIN ON THE BLUE SIDE

      var alliance = DriverStation.getAlliance();
      if (alliance.isPresent()) {
        return alliance.get() == DriverStation.Alliance.Red;
      }
      return false;
    },
    this // Reference to this subsystem to set requirement
  );
}

public void drivePathPlanner(ChassisSpeeds speeds) {
  drive(speeds.vxMetersPerSecond, speeds.vyMetersPerSecond, speeds.omegaRadiansPerSecond, false, this.robot.getPeriod()); //vy and vx swapped
}

public ChassisSpeeds getChassisSpeeds() {
  return this.m_kinematics.toChassisSpeeds(this.getModuleStates());
}

public SwerveDrivePoseEstimator createSwerveDriveEstimator() {
  return new SwerveDrivePoseEstimator(
  Constants.DriveConstants.kDriveKinematics,
  m_gyro.getRotation2d(),
  this.getModulePositions(),
  new Pose2d(),
  stateStdDevs,
  visionMeasurementStdDevs);
}

}
