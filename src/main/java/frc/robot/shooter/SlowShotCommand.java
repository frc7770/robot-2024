package frc.robot.shooter;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;


// turn on shooter and set to certain speed
public class SlowShotCommand extends Command{
  
  public SlowShotCommand() {
    addRequirements(Robot.shooterSubsystem);
  } 

  public void execute(){
    Robot.shooterSubsystem.setLowSpeed();

  }
  
  @Override
  public void end(boolean interrupted){
    Robot.shooterSubsystem.stop();
  }
}
