package frc.robot.shooter;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;

// turn on shooter motors backwards
public class ReverseCommand extends Command{
  public ReverseCommand() {
        addRequirements(Robot.shooterSubsystem);
    }   

    public void execute(){
      Robot.shooterSubsystem.setReverseLowSpeed();
  
    }
    
    @Override
    public void end(boolean interrupted){
      Robot.shooterSubsystem.stop();
    }
}
