package frc.robot.shooter;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * Spins a pair of wheels that launches the note at the target
 * In 2022 we used a MotorControllerGroup, but it is now depricated so we'll
 * just control them seperately for now
 */
public class ShooterSubsystem extends SubsystemBase {
    private RelativeEncoder shooterMainEncoder;
    private RelativeEncoder shooterFollowerEncoder;
    private CANSparkMax shooterMain;
    private CANSparkMax shooterFollower;

    public ShooterSubsystem() {
        shooterMain = new CANSparkMax(Constants.SHOOTER_MAIN, CANSparkLowLevel.MotorType.kBrushless);
        shooterFollower = new CANSparkMax(Constants.SHOOTER_FOLLOWER, CANSparkLowLevel.MotorType.kBrushless);
        shooterMainEncoder = shooterMain.getEncoder();
        shooterFollowerEncoder = shooterFollower.getEncoder();
        shooterMain.setIdleMode(IdleMode.kCoast);
        shooterFollower.setIdleMode(IdleMode.kCoast);
    }

    public void setHighSpeed() {
        shooterMain.set(Constants.SHOT_SPEED);
        shooterFollower.set(Constants.SHOT_SPEED);
    }

    public void setLowSpeed() {
        shooterMain.set(Constants.LOWER_SHOT_SPEED);
        shooterFollower.set(Constants.LOWER_SHOT_SPEED);
        //Robot.intakeSubsystem.setSpeed(-1);
    }

    public void setReverseLowSpeed() {
        shooterMain.set(-Constants.LOWER_SHOT_SPEED);
        shooterFollower.set(-Constants.LOWER_SHOT_SPEED);
    }

    public void stop() {
        shooterMain.set(0);
        shooterFollower.set(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Shooter Main Motor Speed", shooterMainEncoder.getVelocity());
        SmartDashboard.putNumber("Shooter Follower Slave Speed", shooterFollowerEncoder.getVelocity());
    }

}
