package frc.robot.vision;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;

import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.apriltag.AprilTagDetector;
import edu.wpi.first.apriltag.AprilTagPoseEstimator;
import edu.wpi.first.apriltag.AprilTagPoseEstimator.Config;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class AprilTagTagetDetection implements ContourDetection {

    private AprilTagDetector detector = new AprilTagDetector();

    // Set up Pose Estimator - parameters are for a Microsoft Lifecam HD-3000
    // fx camera horizontal focal length, in pixels
    // fy camera vertical focal length, in pixels
    // cx camera horizontal focal center, in pixels
    // cy camera vertical focal center, in pixels
    double cameraFx = 699.377810315881;
    double cameraFy = 677.7161226393544;
    double cameraCx = 345.6059345433618;
    double cameraCy = 207.12741326228522;

    double tagSize = 0.1524; // meters

    // (https://www.chiefdelphi.com/t/wpilib-apriltagdetector-sample-code/421411/21)
    Config poseEstConfig = new AprilTagPoseEstimator.Config(tagSize, cameraFx, cameraFy, cameraCx, cameraCy);

    AprilTagPoseEstimator estimator = new AprilTagPoseEstimator(poseEstConfig);

    public AprilTagTagetDetection() {
        detector.addFamily("tag36h11", 0);
    }

    @Override
    public void process(Mat image) {

        try {
        var grayMat = new Mat();
        Imgproc.cvtColor(image, grayMat, Imgproc.COLOR_RGB2GRAY);

        AprilTagDetection[] detections = detector.detect(grayMat);
        for (AprilTagDetection detection : detections) {
            //System.out.println("MyPipeline: " + detection);
            if(detection.getId() == 4 || detection.getId() == 7) {
                double targetY = detection.getCenterY();
                //Transform3d pose = estimator.estimate(detection);
                //double targetAngle = pose.getRotation().getY();
                SmartDashboard.putNumber("TargetY", targetY);
            }
        }
        } catch(RuntimeException re) {

        }
    }

    @Override
    public ArrayList<MatOfPoint> convexHullsOutput() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'convexHullsOutput'");
    }

}
