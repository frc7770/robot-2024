package frc.robot.vision;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle.Control;

import javax.sound.sampled.Port;

import org.opencv.core.KeyPoint;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.vision.VisionThread;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;

public class VisionThreadSensor {
    private VisionThread visionThread;
    private boolean hasTarget = false;
    private double distance = 0.0;
    private double angle = 0.0;
    private double width = 0.0;
    private Pose2d ringPose = new Pose2d();
    private Field2d ringField = new Field2d();
    public VisionThreadSensor(VideoSource videoSource, frc.robot.vision.ContourDetection videopipeline) {
        visionThread = new VisionThread(videoSource, videopipeline, pipeline -> {    
        if (!pipeline.convexHullsOutput().isEmpty()) {
            Point point = getBestTargetPoint(pipeline.convexHullsOutput());
            width = getSize(pipeline.convexHullsOutput());
            double centeredReading = point.x - 320;
            angle = centeredReading;
            hasTarget = true;
        }
        else {
            hasTarget = false;
            angle = 0;
        }
        SmartDashboard.putBoolean(videoSource.getName() + " has Ring Target", hasTarget);
        SmartDashboard.putNumber(videoSource.getName() + " width of ring", getSize(pipeline.convexHullsOutput()));
        SmartDashboard.putNumber(videoSource.getName() + " angle to ring", getAngle());
        SmartDashboard.putNumber(videoSource.getName() + " distance to ring", getDistance());
        SmartDashboard.putData("Field", ringField);
        ringField.setRobotPose(getRingPose());
        });
        visionThread.start();
    }

    public Point getBestTargetPoint(ArrayList<MatOfPoint> ring) {
        // need a better algorithm.  Maybe find the biggest blob?
        return ring.get(0).toArray()[0];
    }

    public boolean hasTarget() {
        return hasTarget;
    }

    public double getDistance() {
        return Units.inchesToMeters(-0.1225* width + 55.584);
    }

    public double getSize(ArrayList<MatOfPoint> ring) {
        if (ring.size() > 0) {
		    List<Point> corners = ring.get(0).toList();
		    return getWidth(corners);
        }
        return 0;
	}

    private double getWidth(final List<Point> points) {
        double minX = 0;
        double minY = 0;
        double maxX = 0;
        double maxY = 0;
    
    
        for (Point point : points) {
            double x = point.x;
            double y = point.y;
    
            if (minX == 0 || x < minX) {
                minX = x;
                minY = y;
            }
            if (maxX == 0 || x > maxX) {
                maxX = x;
                maxY = y;
            }
        }
        System.out.println(minX);
        System.out.println(maxX);
        if (maxX >= 635 || minX <= 5) {
            hasTarget = false;
            angle = 0;
            distance = 0;
            return 0;
        }
        return maxX - minX;
    }

    public double getAngle() {
        return angle / 10.475;
    }

    public Pose2d getRingPose() {
        Pose2d robotPose = new Pose2d();//Robot.m_swerve.getPose();
        ringPose = new Pose2d();
        return ringPose.plus(new Transform2d(getDistance()*Math.cos(Units.degreesToRadians(getAngle())), -getDistance()*Math.sin(Units.degreesToRadians(getAngle())), new Rotation2d(0)));
    }
}
