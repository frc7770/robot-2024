package frc.robot.climber;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;


// turn on shooter and set to certain speed
public class ClimberUpCommand extends Command{
  
  public ClimberUpCommand() {
    addRequirements(Robot.climberSubsystem);
  } 

  public void execute(){
    Robot.climberSubsystem.setSpeed(-.5);
  }
  
  @Override
  public void end(boolean interrupted){
    Robot.intakeSubsystem.stop();
  }
}
