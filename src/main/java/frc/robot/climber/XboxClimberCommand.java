package frc.robot.climber;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;
import frc.robot.arm.ArmSubsystem;

public class XboxClimberCommand extends Command {
    XboxController copilot;
    public XboxClimberCommand() {
        addRequirements(Robot.climberSubsystem);
        copilot = Robot.m_controller_2;
    }

    @Override
    public void execute() {
        double speed = copilot.getLeftX();
        double speed2 = copilot.getRightX();

        // Ensure Climbers don't go with arms
        if (Math.abs(speed) < Math.abs(copilot.getLeftY())) {
            Robot.climberSubsystem.setLeftSpeed(0);
        }
        if (Math.abs(speed2) < Math.abs(copilot.getRightY())) {
            Robot.climberSubsystem.setRightSpeed(0);
        }

        if (Math.abs(speed) > .15) {
            Robot.climberSubsystem.setLeftSpeed(speed);
        }
        else {
            Robot.climberSubsystem.setLeftSpeed(0);
        }
        if (Math.abs(speed2) > .15) {
            Robot.climberSubsystem.setRightSpeed(-speed2);
        }
        else {
            Robot.climberSubsystem.setRightSpeed(0);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        Robot.climberSubsystem.stop();
    }
}
