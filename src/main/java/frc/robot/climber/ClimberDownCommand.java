package frc.robot.climber;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;


// turn on shooter and set to certain speed
public class ClimberDownCommand extends Command{
  
  public ClimberDownCommand() {
    addRequirements(Robot.climberSubsystem);
  } 

  public void execute(){
    Robot.climberSubsystem.setSpeed(1);
  }
  
  @Override
  public void end(boolean interrupted){
    Robot.intakeSubsystem.stop();
  }
}
