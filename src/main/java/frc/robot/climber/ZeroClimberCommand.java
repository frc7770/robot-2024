package frc.robot.climber;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;

public class ZeroClimberCommand extends Command {
    public ZeroClimberCommand() {
        addRequirements(Robot.climberSubsystem);
    }   

    public void execute(){
      Robot.climberSubsystem.zeroClimbers();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
