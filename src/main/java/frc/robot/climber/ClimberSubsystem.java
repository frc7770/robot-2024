package frc.robot.climber;

import com.revrobotics.CANSparkLowLevel;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class ClimberSubsystem extends SubsystemBase{
    private RelativeEncoder climberLeftEncoder;
    private RelativeEncoder climberRightEncoder;
    private CANSparkMax climberRight;
    private CANSparkMax climberLeft;
    private double leftMaxHeight = 65; // Needs to be refound
    private double rightMaxHeight = 65; // Needs to be refound

    public ClimberSubsystem() {
        climberLeft = new CANSparkMax(Constants.CLIMBER_LEFT, CANSparkLowLevel.MotorType.kBrushless);
        climberLeftEncoder = climberLeft.getEncoder();
        climberLeftEncoder.setPosition(0);
        climberLeft.setIdleMode(IdleMode.kBrake);
        climberRight = new CANSparkMax(Constants.CLIMBER_RIGHT, CANSparkLowLevel.MotorType.kBrushless);
        climberRightEncoder = climberRight.getEncoder();
        climberRightEncoder.setPosition(0);
        climberRight.setIdleMode(IdleMode.kBrake);
    }

    public void setSpeed(double speed){
        /*if (speed < 0 && (getLeftEncoderPosition() < leftMaxHeight || getRightEncoderPosition() < rightMaxHeight)) {
            climberLeft.set(0);
            climberRight.set(0);
        }
        else if (speed > 0 && (getLeftEncoderPosition() > 0 || getRightEncoderPosition() > 0)) {
            climberLeft.set(0);
            climberRight.set(0);
        }
        else { 
            climberLeft.set(speed  * Constants.CLIMBER_SPEED);
            climberRight.set(speed * Constants.CLIMBER_SPEED);
        } */
        //System.out.println(Robot.lbButton2.getAsBoolean());
        /*if(!(Robot.lbButton2.getAsBoolean()) && speed < 0) {
            climberLeft.set(0);
        }*/
        if ((speed > 0 && getLeftEncoderPosition() > leftMaxHeight)) {
            climberLeft.set(0);
        }
        else if ((speed < 0 && getLeftEncoderPosition() < -15)) {
            climberLeft.set(0);
        }
        else {
            climberLeft.set(speed * Constants.CLIMBER_SPEED);
        }

        /*if(!(Robot.rbButton2.getAsBoolean()) && speed < 0) {
            climberRight.set(0);
        }*/
        if ((speed > 0 && getRightEncoderPosition() > rightMaxHeight)) {
            climberRight.set(0);
        }
        else if ((speed < 0 && getRightEncoderPosition() < -15)) {
            climberRight.set(0);
        }
        else {
            climberRight.set(speed * Constants.CLIMBER_SPEED);
        }
    }

    public void setLeftSpeed(double speed) {
        //if ((speed < 0 && getLeftEncoderPosition() < leftMaxHeight)) {
            //climberLeft.set(0);
        //}
        //else {
            climberLeft.set(speed * Constants.CLIMBER_SPEED);
        //}
    }

    public void setRightSpeed(double speed) {
        //if ((speed < 0 && getRightEncoderPosition() < rightMaxHeight)) {
            //climberRight.set(0);
        //}
        //else {
            climberRight.set(speed * Constants.CLIMBER_SPEED);
        //}
    }

    public void stop() {
        climberLeft.set(0);
        climberRight.set(0);
    }

    public double getLeftEncoderPosition() {
        return climberLeftEncoder.getPosition();
    }

    public double getRightEncoderPosition() {
        return climberRightEncoder.getPosition();
    }

    public void zeroClimbers() { 
        climberLeftEncoder.setPosition(0);
        climberRightEncoder.setPosition(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Climber Left Motor Speed", climberLeftEncoder.getVelocity());
        SmartDashboard.putNumber("Climber Right Motor Speed", climberRightEncoder.getVelocity());
        SmartDashboard.putNumber("Climber Left Position", climberLeftEncoder.getPosition());
        SmartDashboard.putNumber("Climber Right Position", climberRightEncoder.getPosition());
    }

}
