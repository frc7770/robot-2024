package frc.robot;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;

public class Constants {
        // Shooter Constants
        public static final int SHOOTER_MAIN = 5;      // CAN ID
        public static final int SHOOTER_FOLLOWER = 12;  // CAN ID
        public static final double SHOT_SPEED = 1;
        public static final double LOWER_SHOT_SPEED = .2;
        // Intake Constants
        public static final int INTAKE_MAIN = 11;
        public static final double INTAKE_SPEED = .75;
        // Climber Constants
        public static final int CLIMBER_LEFT = 20;
        public static final int CLIMBER_RIGHT = 31;
        public static final double CLIMBER_SPEED = .5;
        // Arm Constants
        public static final int ARM_MAIN = 15;
        public static final int ARM_FOLLOWER = 60;
        public static final double MAIN_ARM_SPEED = .125;
        public static final double FOLLOWER_ARM_SPEED = .15;
        public static final double ARM_MAIN_ENCODER_CONVERSION = 1; //Tune to the encoder clicks per degree
        public static final double ARM_FOLLOWER_ENCODER_CONVERSION = 1;
        public static final double ARM_MAIN_SPEED_CONVERSION = 2.5; //Tune to the encoder clicks per degree
        public static final double ARM_FOLLOWER_SPEED_CONVERSION = 2.5;


        // SWERVE
    public static final int DRIVE_MOTOR_LEFT_FRONT = 29;
    public static final int TURNING_MOTOR_LEFT_FRONT = 32;
    public static final int TURNING_ENCODER_LEFT_FRONT = 1;

    public static final int DRIVE_MOTOR_LEFT_REAR = 27;
    public static final int TURNING_MOTOR_LEFT_REAR = 24;
    public static final int TURNING_ENCODER_LEFT_REAR = 4;

    public static final int DRIVE_MOTOR_RIGHT_FRONT = 26; //26
    public static final int TURNING_MOTOR_RIGHT_FRONT = 22;
    public static final int TURNING_ENCODER_RIGHT_FRONT = 2;
    
    public static final int DRIVE_MOTOR_RIGHT_REAR = 25;
    public static final int TURNING_MOTOR_RIGHT_REAR = 23; //33
    public static final int TURNING_ENCODER_RIGHT_REAR = 3;

    //Starting positions of swerve encoders
    public static final double FRONTLEFT_STARTINGPOSITION = 0;//-0.139892578125;
    public static final double FRONTRIGHT_STARTINGPOSITION = 0;//0.28466796875;
    public static final double BACKLEFT_STARTINGPOSITION = 0;//-0.41796875;
    public static final double BACKRIGHT_STARTINGPOSITION = 0;//0.284912109375;
    //Starting positions of swerve encoders in radians
    public static final double FRONTLEFT_STARTINGRADIANS = FRONTLEFT_STARTINGPOSITION * 2 * Math.PI;
    public static final double BACKLEFT_STARTINGRADIANS = BACKLEFT_STARTINGPOSITION * 2 * Math.PI;
    public static final double FRONTRIGHT_STARTINGRADIANS = FRONTRIGHT_STARTINGPOSITION * 2 * Math.PI;
    public static final double BACKRIGHT_STARTINGRADIANS = BACKRIGHT_STARTINGPOSITION * 2 * Math.PI;

     public static final class DriveConstants {

        public static final double kTrackWidth = Units.inchesToMeters(21);
        // Distance between right and left wheels
        public static final double kWheelBase = Units.inchesToMeters(25.5);
        // Distance between front and back wheels
        public static final SwerveDriveKinematics kDriveKinematics = new SwerveDriveKinematics(
                new Translation2d(kWheelBase / 2, -kTrackWidth / 2),
                new Translation2d(kWheelBase / 2, kTrackWidth / 2),
                new Translation2d(-kWheelBase / 2, -kTrackWidth / 2),
                new Translation2d(-kWheelBase / 2, kTrackWidth / 2));

        

        public static final double kPhysicalMaxSpeedMetersPerSecond = 5;
        public static final double kPhysicalMaxAngularSpeedRadiansPerSecond = 2 * 2 * Math.PI;

        public static final double kTeleDriveMaxSpeedMetersPerSecond = kPhysicalMaxSpeedMetersPerSecond / 4;
        public static final double kTeleDriveMaxAngularSpeedRadiansPerSecond = //
                kPhysicalMaxAngularSpeedRadiansPerSecond / 4;
        public static final double kTeleDriveMaxAccelerationUnitsPerSecond = 4.5;
        public static final double kTeleDriveMaxAngularAccelerationUnitsPerSecond = 3;
    }

    public static final class AutoConstants {
        public static final double kMaxSpeedMetersPerSecond = DriveConstants.kPhysicalMaxSpeedMetersPerSecond / 4;
        public static final double kMaxAngularSpeedRadiansPerSecond = //
                DriveConstants.kPhysicalMaxAngularSpeedRadiansPerSecond / 10;
        public static final double kMaxAccelerationMetersPerSecondSquared = 3;
        public static final double kMaxAngularAccelerationRadiansPerSecondSquared = Math.PI / 4;
        public static final double kPXController = 1.5;
        public static final double kPYController = 1.5;
        public static final double kPThetaController = 3;

        public static final TrapezoidProfile.Constraints kThetaControllerConstraints = //
                new TrapezoidProfile.Constraints(
                        kMaxAngularSpeedRadiansPerSecond,
                        kMaxAngularAccelerationRadiansPerSecondSquared);
    }

}