# Robot 2024
## FRC 7770 - Infinite Voltage

# CRESCENDO
## Design and Challenge

In CRESCENDO presented by Haas, two competing alliances are invited to score notes, amplify their speaker, harmonize onstage, and take the spotlight before time runs out. Alliances earn additional rewards for meeting specific scoring thresholds and for cooperating with their opponents.
- During the first 15 seconds of the match, robots are autonomous. Without guidance from their drivers, robots leave their starting zone, score notes in their speaker or amp, and collect and score additional notes.
- During the remaining 2 minutes and 15 seconds, drivers control their robots. Robots collect notes from human players at their source and score them in their amp and speaker. Each time an alliance gets 2 notes in their amp, the human player can amplify their speaker for 10 seconds. Notes scored in an amplified speaker are worth more points than those scored in an unamplified speaker.
- A human player may choose to repurpose a note scored in their amp in cooperation with their opponent. If each alliance repurposes a note by hitting their Coopertition button in the first 45 seconds of teleop, all teams in the match receive a Coopertition point (which influences their rank in the tournament), and the number of notes needed for the melody bonus is reduced.
- As time runs out, robots race to get onstage and deliver notes to their traps. Harmonizing robots, i.e. robots sharing a chain, earn an added bonus. Robots earn even more points if a human player spotlights robots on a chain by scoring a note on the chain’s microphone.

The alliance that earns the most points wins the match!

## Subsystems
### Drive
Swerve using
* WestCoast Products SwerveX
* REV Robotics Neo and SparkMax
* CTR CANcoder
* Kauai NavX
### Shooter
Double fly wheel.
### Intake
Over the bumper tri-roller system.

## Controls
### XBox Driver

## Vision Coprocessor
See "Vision 2024" project

The code that runs on the RaspberryPi WPILibPi coprocessor and shares info back to the Network Tables.

```
.\gradlew build -Pvision 
// creates build/libs/robot-2024-raspberrypi.jar
```
Then upload through the WPILibPi interface.
